mod peg;
mod decl;

pub use decl::*;


#[cfg(test)]
mod tests {
    #[cfg(test)]
    mod vars {
        use peg;
        use decl::*;

        #[test]
        fn plain() {
            let parsed = peg::var(":x").unwrap();
            assert!(parsed.name == "x");
            assert!(parsed.mode == Variable);
            assert!(!parsed.list);
        }

        #[test]
        fn ident() {
            let parsed = peg::var("&table").unwrap();
            assert!(parsed.name == "table");
            assert!(parsed.mode == Identifier);
            assert!(!parsed.list);
        }

        #[test]
        fn immediate() {
            let parsed = peg::var("!y").unwrap();
            assert!(parsed.name == "y");
            assert!(parsed.mode == Literal);
            assert!(!parsed.list);
        }

        #[test]
        fn plain_arr() {
            let parsed = peg::var(":x*").unwrap();
            assert!(parsed.name == "x");
            assert!(parsed.mode == Variable);
            assert!(parsed.list);
        }

        #[test]
        fn ident_arr() {
            let parsed = peg::var("&table*").unwrap();
            assert!(parsed.name == "table");
            assert!(parsed.mode == Identifier);
            assert!(parsed.list);
        }

        #[test]
        fn immediate_arr() {
            let parsed = peg::var("!y*").unwrap();
            assert!(parsed.name == "y");
            assert!(parsed.mode == Literal);
            assert!(parsed.list);
        }
    }

    #[cfg(test)]
    mod  decls {
        use peg;
        use decl::*;

        #[test]
        fn simple() {
            let parsed = peg::declaration("\n--@ name\nSELECT now();\n");
            let parsed = parsed.unwrap();
            assert!(parsed.name == "name");
        }

        #[test]
        fn extra_whitespace() {
            let parsed = peg::declaration("\n--@ name\n\nSELECT now();\n  \n");
            let parsed = parsed.unwrap();
            assert!(parsed.name == "name");
        }

        #[test]
        fn one_var() {
            let parsed = peg::declaration("\n--@ name\nSELECT now() + :t;\n");
            let parsed = parsed.unwrap();
            assert!(parsed.name == "name");
            assert!(parsed.vars[0].name == "t");
            assert!(parsed.vars[0].mode == Variable);
        }

        #[test]
        fn many_var() {
            let parsed = peg::declaration("\n--@ the_query ro\n
                                           SELECT &col1 + :v1,
                                                  &col2 + :v2,
                                                  &col2 + :v3
                                             FROM &tab;\n");
            let parsed = parsed.unwrap();
            assert!(parsed.name == "the_query");
            println!("Decl: {:?}", parsed);
        }

        #[test]
        fn types() {
            let parsed = peg::declaration("\n--@ the_query ro\n
                                           SELECT &col1::cube,
                                                  &col2 :: line,
                                                  &col2 + :v3::integer
                                             FROM &tab;\n");
            let parsed = parsed.unwrap();
            assert!(parsed.name == "the_query");
            println!("Decl: {:?}", parsed);
        }
    }
}
