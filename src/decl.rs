/*
#[derive(Debug)]
pub struct Query<'a> {
    pub name: &'a str,
    pub ro: bool,
    pub text: &'a str,
    pub idents: BTreeMap<&str, (usize, usize), >
    pub vars: Vec<Var<'a>>,
}
 */


#[derive(Debug)]
pub struct Decl<'a> {
    pub name: &'a str,
    pub ro: bool,
    pub vars: Vec<Var<'a>>,
    pub text: &'a str,
}


#[derive(Debug)]
pub struct Var<'a> {
  pub pos: (usize, usize),
  pub name: &'a str,
  pub mode: Mode,
  pub list: bool,
}



#[derive(Debug, Eq, PartialEq)]
pub enum Mode {
    Variable,
    Literal,
    Identifier
}

pub use Mode::*;

impl Mode {
    pub fn parse(s: &str) -> Option<Mode> {
        match s {
            ":" => Some(Variable),
            "&" => Some(Identifier),
            "!" => Some(Literal),
            _   => None,
        }
    }
}
